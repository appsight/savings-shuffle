//
//  RequestModel.h
//  OnePercent
//
//  Created by Umer Hassam on 19/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MKNetworkEngine.h"

// do a typedef for the block
typedef void (^IDBlock)(id object);
typedef void (^completionBlock)(BOOL success);


@interface RequestModel : MKNetworkEngine

-(void)showAlertWithMessage:(NSString *)message;
-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

-(BOOL)requestSuccessful:(NSDictionary *)dictionary;
-(BOOL)userSignedIn;

-(NSDictionary *)retrieveUserData;

-(void)signUpWithUsername:(NSString *)username Email:(NSString *)email password:(NSString *)password completionHandler:(completionBlock)block;
-(void)signInWithEmail:(NSString *)email password:(NSString *)password completionHandler:(completionBlock)block;
-(void)getProfileDetail:(NSString *)idd completionHandler:(IDBlock)completionBlock;
-(void)updateProfileWithUsername:(NSString *)username password:(NSString *)password completionHandler:(completionBlock)block;
-(void)forgotPasswordWithEmail:(NSString *)email completionHandler:(completionBlock)block;

-(void)getDeckList:(NSString *)idd completionHandler:(IDBlock)completionBlock;
-(void)getCardList:(NSString *)idd DeckId:(NSString *)deckid completionHandler:(IDBlock)completionBlock;
-(void)getUsedCardList:(NSString *)idd completionHandler:(IDBlock)completionBlock;
-(void)redeemCard:(NSString *)idd CardId:(NSString *)cardid completionHandler:(IDBlock)completionBlock;
@end

