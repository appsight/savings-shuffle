//
//  AppDelegate.h
//  OnePercent
//
//  Created by Umer Hassam on 09/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RequestModel *mc;

@end

