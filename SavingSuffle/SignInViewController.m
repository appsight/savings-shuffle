//
//  SignInViewController.m
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "SignInViewController.h"

#import "RequestModel.h"

@interface SignInViewController () <UITextFieldDelegate>

@property (nonatomic, retain) NSMutableData *responseData;

@property (nonatomic) BOOL viewUP;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self retrieveUserData]){
      [self performSegueWithIdentifier:@"HomeViewController" sender:self];
    }

}
-(NSDictionary *)retrieveUserData{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return (NSDictionary *) [userDefaults valueForKey:@"userData"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - IBActions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)signInButtonPressed:(id)sender {

    
    NSString *username = ([_usernameField.text isEqualToString:_usernameField.placeholder]) ? @"" : _usernameField.text;
    NSString *password =([_passwordField.text isEqualToString:_passwordField.placeholder]) ? @"" : _passwordField.text;
    
    if ([username isEqualToString:@""] || [password isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Form Incomplete!"
                                                        message:@"Please fillout all the fields before proceeding."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    RequestModel *requestModel = [[RequestModel alloc] init];
    
    [requestModel signInWithEmail:_usernameField.text password:_passwordField.text completionHandler:^(BOOL success){
        if(success){
        

            [_usernameField setText:@""];
            [_passwordField setText:@""];
            
            [self performSegueWithIdentifier:@"HomeViewController" sender:self];
        
        }
    }];
    
}

//- (IBAction)signInButtonPressed:(id)sender {
//}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField.text isEqualToString:textField.placeholder]){
        textField.text = @"";
    }
    
    //[self moveViewUp];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        textField.text = textField.placeholder;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    //[self moveViewDown];
    [textField resignFirstResponder];
    
    
    return NO;
}

#pragma mark - View Animations
-(void)moveViewUp{
    if(_viewUP) return;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y - 200,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = YES;
    }];
}

-(void)moveViewDown{
    if(!_viewUP) return;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y + 200,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = NO;
    }];
}

@end
