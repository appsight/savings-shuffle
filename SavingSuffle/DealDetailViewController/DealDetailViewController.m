//
//  DealDetailViewController.m
//  OnePercent
//
//  Created by ajay desai on 25/03/15.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "DealDetailViewController.h"
#import "RequestModel.h"
#import "UIImageView+WebCache.h"
@interface DealDetailViewController ()
{
    RequestModel *requestModel;
}
@end

@implementation DealDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"cardId:%@",_cardDict);
    
    [_header_lbl setText:[_cardDict objectForKey:@"title"]];
    [_card_address setText:[_cardDict objectForKey:@"address"]];
    [_cardImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UHA_URL_BASE_URL,[_cardDict objectForKey:@"image"]]]];
    //[_offer_title_lbl setText:[_cardDict objectForKey:@"offertext"]];
    //[_desclamer_lbl setText:[_cardDict objectForKey:@"description"]];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}
- (IBAction)redeemBtn:(UIButton *)sender {
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Are you sure you want to use this deal?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert setTag:1];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag==1) {
        
        if (buttonIndex==0) {
            NSLog(@"0");
            
        
            
        }else{
            
            if (!requestModel) {
                requestModel = [[RequestModel alloc] init];
            }
            [requestModel redeemCard:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] CardId:[_cardDict objectForKey:@"id"] completionHandler:^(id dictionary){
                if (dictionary) {
                    NSLog(@"dictionary:%@",dictionary);
                    [[self navigationController] popViewControllerAnimated:YES];
                }
            }];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
