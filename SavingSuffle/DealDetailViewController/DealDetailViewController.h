//
//  DealDetailViewController.h
//  OnePercent
//
//  Created by ajay desai on 25/03/15.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *header_lbl;
@property (weak, nonatomic) IBOutlet UIImageView *cardImg;
@property (weak, nonatomic) IBOutlet UILabel *card_address;
@property (weak, nonatomic) IBOutlet UILabel *offer_title_lbl;
@property (weak, nonatomic) IBOutlet UILabel *desclamer_lbl;
@property (nonatomic,retain) NSDictionary *cardDict;
@end
