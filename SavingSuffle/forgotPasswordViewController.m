//
//  forgotPasswordViewController.m
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "forgotPasswordViewController.h"
#import "RequestModel.h"

@interface forgotPasswordViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailField;

@property (nonatomic) BOOL viewUP;
@end

@implementation forgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelButtonPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Are You Sure"
                                                    message:@"Are you sure you wish to cancel this action."
                                                   delegate:self
                                          cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];
    [alert show];
}
- (IBAction)submitButtonPressed:(id)sender {
    RequestModel *requestModel = [[RequestModel alloc] init];
    
    if ([_emailField.text length]>0) {
        
            if ([self validateEmail:_emailField.text]) {
                [requestModel forgotPasswordWithEmail:_emailField.text completionHandler:^(BOOL success){
                    if(success){
                       [self.navigationController popViewControllerAnimated:YES];
                        
                    }
                }];

            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Enter Valid Email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
                
            }
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"Please Enter Your Email." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
  
}
- (BOOL) validateEmail: (NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        // YES pressed
        [self.navigationController popViewControllerAnimated:YES];
        
    } else {
        // NO pressed
        // Do noting
    }
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField.text isEqualToString:textField.placeholder]){
        textField.text = @"";
    }
    
    //[self moveViewUp];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        textField.text = textField.placeholder;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    //[self moveViewDown];
    [textField resignFirstResponder];
    
    
    return NO;
}


#pragma mark - View Animations
-(void)moveViewUp{
    if(_viewUP) return;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y - 100,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = YES;
    }];
}

-(void)moveViewDown{
    if(!_viewUP) return;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y + 100,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = NO;
    }];
}
@end
