//
//  SignInViewController.h
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)signInButtonPressed:(id)sender;

@end
