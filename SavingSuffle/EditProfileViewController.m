//
//  EditProfileViewController.m
//  OnePercent
//
//  Created by Umer Hassam on 12/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "EditProfileViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "RequestModel.h"

@interface EditProfileViewController () <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic) BOOL viewUP;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    RequestModel *requestModel = [[RequestModel alloc] init];
    [requestModel getProfileDetail:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] completionHandler:^(id dictionary){
        if (dictionary) {
            NSLog(@"dictionary:%@",dictionary);
            if ([[[dictionary objectForKey:@"User"] objectForKey:@"username"] length]>0) {
                [_nameField setText:[[dictionary objectForKey:@"User"] objectForKey:@"username"]];
            }else{
                [_nameField setText:[NSString stringWithFormat:@"%@ %@",[[dictionary objectForKey:@"User"] objectForKey:@"first_name"],[[dictionary objectForKey:@"User"] objectForKey:@"last_name"]]];
            }
            
        }
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBActions

- (IBAction)backButtonPressed:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}
- (IBAction)usedDealBtn:(UIButton *)sender {
    [self performSegueWithIdentifier:@"UsedDealsListViewController" sender:nil];
}
- (IBAction)signoutBtn:(UIButton *)sender {
     [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
    [[self navigationController] popToRootViewControllerAnimated:YES];
}



- (IBAction)updateButtonPressed:(id)sender {
   
    
    
    if (_nameField.text == nil || [_nameField.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Your UserName" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    

    
    else if  (![_currentPasswordField.text isEqualToString:_nPasswordField.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Password Not Matching" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
    }
    
    
    else {
        RequestModel *requestModel = [[RequestModel alloc] init];
       
        if ([_nPasswordField.text length]>0) {
            [requestModel updateProfileWithUsername:_nameField.text password:_nPasswordField.text completionHandler:^(BOOL success){
                if (success) {
                    
                }
            }];
        }else{
            [requestModel updateProfileWithUsername:_nameField.text password:nil completionHandler:^(BOOL success){
                if (success) {
                    
                }
            }];
        }

    }
    
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0) {
        
    } else if(buttonIndex == 1){
        [self displayCamera];
    } else {
        NSLog(@"Cancel");
    }
}
-(void)displayCamera{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, kUTTypeImage, nil];
        imagePickerController.delegate = self;
        imagePickerController.editing = YES;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
    else
    {
        //[self showAlertWithTitle:@"Info" message:@"Camera not available" delegate:nil];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Camera Not Available"
                                                        message:@"The camera is not available on this device."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
        [alert show];
    }
    
}



-(void)imageSelected:(UIImage *)image{
    _profileImage.image = image;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField.text isEqualToString:textField.placeholder]){
        textField.text = @"";
    }
    
    //[self moveViewUp];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        textField.text = textField.placeholder;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    //[self moveViewDown];
    [textField resignFirstResponder];
    
    
    return NO;
}



#pragma mark - View Animations
-(void)moveViewUp{
    if(_viewUP) return;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y - 250,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = YES;
    }];
}

-(void)moveViewDown{
    if(!_viewUP) return;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y + 250,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = NO;
    }];
}

@end
