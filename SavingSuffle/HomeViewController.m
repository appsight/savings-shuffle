
//
//  HomeViewController.m
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "HomeViewController.h"
#import "DealsListViewController.h"

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate>

{
    UITableViewController *tableViewController;
    RequestModel *requestModel;
    NSMutableArray *DeckArray;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    //[self.mytable addSubview:refreshControl];
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableview;
    tableViewController.refreshControl = refreshControl;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!requestModel) {
        requestModel = [[RequestModel alloc] init];
    }
    [requestModel getDeckList:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] completionHandler:^(id dictionary){
        if (dictionary) {
            NSLog(@"dictionary:%@",dictionary);
            
            DeckArray=[[NSMutableArray   alloc]initWithArray:[dictionary objectForKey:@"Deck"]];
            [_tableview reloadData];
        }
    }];
}

-(void)refreshData
{
    //Put your logic here
    [tableViewController.refreshControl endRefreshing];
    
    if (!requestModel) {
        requestModel = [[RequestModel alloc] init];
    }
    [requestModel getDeckList:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] completionHandler:^(id dictionary){
        if (dictionary) {
            NSLog(@"dictionary:%@",dictionary);
            DeckArray=[[NSMutableArray   alloc]initWithArray:[dictionary objectForKey:@"Deck"]];
            [_tableview reloadData];

            
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)goProfileBtn:(UIButton *)sender {
    [self performSegueWithIdentifier:@"EditProfileViewController" sender:nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"DealsListViewController"])
    {
        // Get reference to the destination view controller
        DealsListViewController *vc = [segue destinationViewController];
        // Pass any objects to the view controller here, like...
        [vc setDeckId:[[DeckArray objectAtIndex:[sender intValue]] objectForKey:@"id"]];
    }
}


#pragma mark - UITableviewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [DeckArray count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"DeckCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    UILabel *nameLabel = (UILabel *)[cell viewWithTag:1];
    
    nameLabel.text = [[DeckArray objectAtIndex:indexPath.row] objectForKey:@"title"];

    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"DealsListViewController" sender:[NSNumber numberWithInt:indexPath.row]];
}




@end
