//
//  RequestModel.m
//  OnePercent
//
//  Created by Umer Hassam on 19/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "RequestModel.h"

#import "MKNetworkOperation.h"
#import "MBProgressHUD.h"


#define UHA_URL_SIGN_UP         @"/suffle/api/signup"
#define UHA_URL_SIGN_IN         @"/suffle/api/login"
#define UHA_URL_PROFILE_DETAIL  @"/suffle/api/Myprofile"
#define UHA_URL_PROFILE_UPDATE  @"/suffle/api/updateprofile"
#define UHA_URL_FORGOT_PASSWORD @"/suffle/api/forgotpassword"
#define UHA_URL_DECK_LIST       @"/suffle/api/decklist"
#define UHA_URL_CARD_LIST       @"/suffle/api/cardlist"
#define UHA_URL_REDEEM_CARD     @"/suffle/api/redeem"
#define UHA_URL_USED_CARD_LIST  @"/suffle/api/usedlist"
@interface RequestModel(){
    
}

@property (strong, nonatomic) MKNetworkOperation *uploadOperation;
@property (nonatomic, strong) RequestModel *requestModel;

@end


@implementation RequestModel

- (id)init
{
    self = [super initWithHostName:@"www.fluxtechsolutions.com"];
    if (self) {
        //_requestModel = [[RequestModel alloc] init];
    }
    
    return self;
}

#pragma mark - Private Methods

-(MKNetworkOperation*) postRequest:(NSString*)path Params:(NSMutableDictionary*)params
                 completionHandler:(IDBlock) completionBlock
                      errorHandler:(MKNKErrorBlock) errorBlock {
    //[MBProgressHUD showHUDAddedTo:[DELEGATE window] animated:YES].detailsLabelText = @"Please wait...";
    //MBProgressHUD *mbp = [MBProgressHUD showHUDAddedTo:[DELEGATE.window] animated:YES]
    
    UIImage *photo=nil;
    if ([params objectForKey:@"photo"]) {
        photo=[params objectForKey:@"photo"];
        [params removeObjectForKey:@"photo"];
    }
    
    MKNetworkOperation *operation = [self operationWithPath:path
                                                     params: params
                                                 httpMethod:@"POST"];
    
    if (photo) {
        
        NSData *data = UIImageJPEGRepresentation(photo,0.5);
        [operation addData:data forKey:@"photo" mimeType:@"image/jpeg" fileName:@"upload.jpg"];
        
        //setFreezable uploads your images after connection is restored!
        [operation setFreezable:YES];
        
    }
    
    //  [operation setPostDataEncoding:MKNKPostDataEncodingTypeJSON];
    [operation addCompletionHandler:^(MKNetworkOperation* completedOperation) {
        
        //[MBProgressHUD hideAllHUDsForView:[DELEGATE window] animated:YES];
        id responceDict = [completedOperation responseJSON];
        
        completionBlock(responceDict);
    }
                       errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
                           //[MBProgressHUD hideAllHUDsForView:[DELEGATE window] animated:YES];
                           errorBlock(error);
                       }];
    
    [self enqueueOperation:operation];
    return operation;
    
}

-(void)saveUserData:(NSDictionary *)dictionary{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:dictionary forKey:@"userData"];
    [userDefaults synchronize];
}

#pragma mark - Public Methods


-(NSDictionary *)retrieveUserData{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    return (NSDictionary *) [userDefaults valueForKey:@"userData"];
   
}

-(void)showAlertWithMessage:(NSString *)message{
    [self showAlertWithTitle:@"Alert!" message:message];
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message{
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:title
                                                  message:message
                                                 delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil, nil];
    
    [alert show];
}

-(BOOL)requestSuccessful:(NSDictionary *)dictionary{
    return [[dictionary valueForKey:@"successful"] boolValue];
}

-(BOOL)userSignedIn{
    return (nil != [self retrieveUserData]) ? YES : NO;
}

-(void)signUpWithUsername:(NSString *)username Email:(NSString *)email password:(NSString *)password completionHandler:(completionBlock)block{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   username, @"username",
                                   password, @"password",
                                   email, @"email",nil];
    
    _uploadOperation = [self postRequest:UHA_URL_SIGN_UP Params:params
                               completionHandler:^(id responceDict) {
                                
                                   if ([self requestSuccessful:responceDict]) {
                                         [self showAlertWithTitle:@"Congratulations!" message:@"You have successfully registered."];
                                        if(nil != block) block([self requestSuccessful:responceDict]);
                                        [self saveUserData:responceDict];
                                   }else{
                                       [self showAlertWithTitle:@"Alert!" message:[responceDict objectForKey:@"message"]];
                                   }
                    
                               }
                                errorHandler:^(NSError* error) {
                                    
                                [UIAlertView showWithError:error];
                            }];
    
}

-(void)signInWithEmail:(NSString *)email password:(NSString *)password completionHandler:(completionBlock)block{
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   password, @"password",
                                   email, @"email",nil];
    
    _uploadOperation = [self postRequest:UHA_URL_SIGN_IN Params:params
                       completionHandler:^(id responceDict) {
                           
                           if ([self requestSuccessful:responceDict]) {
                               if(nil != block) block([self requestSuccessful:responceDict]);
                               [self saveUserData:responceDict];
                           }else{
                               [self showAlertWithTitle:@"Error!" message:[responceDict objectForKey:@"message"]];
                           }
                           
                           NSLog(@"%@",responceDict);
                           
                       } errorHandler:^(NSError* error) {
                           
                           [UIAlertView showWithError:error];
                           
                       }];
    
}


-(void)getProfileDetail:(NSString *)idd completionHandler:(IDBlock)completionBlock{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   idd, @"userid",nil];
    
    _uploadOperation = [self postRequest:UHA_URL_PROFILE_DETAIL Params:params
                       completionHandler:^(id responceDict) {
                           if(nil != completionBlock) completionBlock(responceDict);
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    
}

-(void)getDeckList:(NSString *)idd completionHandler:(IDBlock)completionBlock{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   idd, @"userid",nil];
    
    _uploadOperation = [self postRequest:UHA_URL_DECK_LIST Params:params
                       completionHandler:^(id responceDict) {
                           
                           if(nil != completionBlock) completionBlock(responceDict);
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    
}

-(void)getCardList:(NSString *)idd DeckId:(NSString *)deckid completionHandler:(IDBlock)completionBlock{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   idd, @"userid",deckid,@"deckid", nil];
    
    _uploadOperation = [self postRequest:UHA_URL_CARD_LIST Params:params
                       completionHandler:^(id responceDict) {
                           if(nil != completionBlock) completionBlock(responceDict);
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    
}

-(void)getUsedCardList:(NSString *)idd completionHandler:(IDBlock)completionBlock{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   idd, @"userid", nil];
    
    _uploadOperation = [self postRequest:UHA_URL_USED_CARD_LIST Params:params
                       completionHandler:^(id responceDict) {
                           if(nil != completionBlock) completionBlock(responceDict);
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    
}

-(void)redeemCard:(NSString *)idd CardId:(NSString *)cardid completionHandler:(IDBlock)completionBlock{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   idd, @"userid",cardid,@"card_id", nil];
    
    _uploadOperation = [self postRequest:UHA_URL_REDEEM_CARD Params:params
                       completionHandler:^(id responceDict) {
                           if(nil != completionBlock) completionBlock(responceDict);
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    
}


-(void)updateProfileWithUsername:(NSString *)username password:(NSString *)password completionHandler:(completionBlock)block{
    
    // Failsafe... can't let the user save an empty password
    if ([password isEqualToString:@""]) {
        password = nil;
    }
    
    NSString *user_id = [[self retrieveUserData] valueForKey:@"id"];

    

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   user_id, @"userid",
                                   username, @"username",
                                   password, @"password", nil];
  
    _uploadOperation = [self postRequest:UHA_URL_PROFILE_UPDATE Params:params
                       completionHandler:^(id responceDict) {
                           
                           
                           
                           if ([self requestSuccessful:responceDict]) {
                               [self showAlertWithTitle:@"Alert!" message:@"Your profile has been successfully updated."];
                                if(nil != block) block([self requestSuccessful:responceDict]);
                           }else{
                               [self showAlertWithTitle:@"Alert!" message:[responceDict valueForKey:@"message"]];
                           }
                           
                          
                           
                       }
                            errorHandler:^(NSError* error) {
                                
                                [UIAlertView showWithError:error];
                            }];
    

}

-(void)forgotPasswordWithEmail:(NSString *)email completionHandler:(completionBlock)block{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   email, @"email",nil];
    
    _uploadOperation = [self postRequest:UHA_URL_FORGOT_PASSWORD Params:params
                       completionHandler:^(id responceDict) {
                           
                           [self showAlertWithTitle:@"Alert!" message:[responceDict valueForKey:@"message"]];
                           
                           if(nil != block) block([self requestSuccessful:responceDict]);
                           
                           
                       } errorHandler:^(NSError* error) {
                           
                           [UIAlertView showWithError:error];
                           
                       }];
}

@end
