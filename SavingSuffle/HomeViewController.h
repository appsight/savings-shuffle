//
//  HomeViewController.h
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RequestModel.h"
@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
