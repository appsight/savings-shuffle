//
//  UsedDealsListViewController.m
//  OnePercent
//
//  Created by ajay desai on 25/03/15.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "UsedDealsListViewController.h"
#import "RequestModel.h"
#import "DealTableViewCell.h"
#import "UIImageView+WebCache.h"
@interface UsedDealsListViewController ()
{
    UITableViewController *tableViewController;
    RequestModel *requestModel;
    NSMutableArray *CardArray;
}
@end

@implementation UsedDealsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    //[self.mytable addSubview:refreshControl];
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableview;
    tableViewController.refreshControl = refreshControl;

    
    if (!requestModel) {
        requestModel = [[RequestModel alloc] init];
    }
    
    [requestModel getUsedCardList:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] completionHandler:^(id dictionary){
        if (dictionary) {
            NSLog(@"dictionary:%@",dictionary);
            CardArray=[[NSMutableArray alloc]initWithArray:[dictionary objectForKey:@"Card"]];
            [_tableview reloadData];        }
    }];
    // Do any additional setup after loading the view.
}

-(void)refreshData
{
    //Put your logic here
    [tableViewController.refreshControl endRefreshing];
    
    if (!requestModel) {
        requestModel = [[RequestModel alloc] init];
    }
    
    [requestModel getUsedCardList:[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"] completionHandler:^(id dictionary){
        if (dictionary) {
            NSLog(@"dictionary:%@",dictionary);
            CardArray=[[NSMutableArray alloc]initWithArray:[dictionary objectForKey:@"Card"]];
            [_tableview reloadData];        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonPressed:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
    
}


#pragma mark - UITableviewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [CardArray count];
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *CellIdentifier = @"DealsCell";
    
    DealTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[DealTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    [cell.dealTitle setText:[[CardArray objectAtIndex:indexPath.row] objectForKey:@"title"]];
    
    [cell.dealImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UHA_URL_BASE_URL,[[CardArray objectAtIndex:indexPath.row] valueForKey:@"image"]]]];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[[CardArray objectAtIndex:indexPath.row] objectForKey:@"redeem_date"] doubleValue]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yy HH:MM"];
    
    [cell.Date_lbl setText:[[[formatter stringFromDate:date] componentsSeparatedByString:@" "] objectAtIndex:0]];
    [cell.Time_lbl setText:[[[formatter stringFromDate:date] componentsSeparatedByString:@" "] objectAtIndex:1]];
//    NSLog(@"Timestamp:%@  redeem_date:%@",[[CardArray objectAtIndex:indexPath.row] objectForKey:@"redeem_date"] ,[formatter stringFromDate:date]);
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setUserInteractionEnabled:NO];
    return cell;
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    [self performSegueWithIdentifier:@"DealDetailViewController" sender:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
