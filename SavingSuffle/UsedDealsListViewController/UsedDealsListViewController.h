//
//  UsedDealsListViewController.h
//  OnePercent
//
//  Created by ajay desai on 25/03/15.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsedDealsListViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end
