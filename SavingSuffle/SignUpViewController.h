//
//  SignUpViewController.h
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"
@interface SignUpViewController : UIViewController
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end
