//
//  SignUpViewController.m
//  OnePercent
//
//  Created by Umer Hassam on 11/03/2015.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import "SignUpViewController.h"

#import "RequestModel.h"

@interface SignUpViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordField;

@property (nonatomic) BOOL viewUP;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [_scrollView setContentSize:CGSizeMake(320, 480)];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - IBActions

- (IBAction)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL) validateEmail: (NSString *)candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

- (IBAction)signUpButtonPressed:(id)sender {
    

    
    if (_usernameField.text == nil || [_usernameField.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Your UserName" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
    }
    
    else if  (_emailField.text == nil|| [_emailField.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Your Email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
    }
    
    else if  (![self validateEmail:_emailField.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Valid Email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
      
        
    }
    
    else if  (_passwordField.text == nil|| [_passwordField.text isEqualToString:@""]) {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
       
        
    }
    
    else if  (_confirmPasswordField.text == nil|| [_confirmPasswordField.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Conform Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
      
        
    }
    
    else if  (![_confirmPasswordField.text isEqualToString:_passwordField.text]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Password Not Matching" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
       
        
    }
   
    
    else {
          RequestModel *requestModel = [[RequestModel alloc] init];
        [requestModel signUpWithUsername:_usernameField.text Email:_emailField.text password:_passwordField.text completionHandler:^(BOOL success){
            if (success) {
                 [self performSegueWithIdentifier:@"HomeViewController" sender:self];
            }
        }];
    }

    
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if([textField.text isEqualToString:textField.placeholder]){
        textField.text = @"";
    }
    
    //[self moveViewUp];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text isEqualToString:@""]){
        textField.text = textField.placeholder;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    //[self moveViewDown];
    [textField resignFirstResponder];
    
    
    return NO;
}


#pragma mark - View Animations

-(void)moveViewUp{
    if(_viewUP) return;
    
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y - 220,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = YES;
    }];
}

-(void)moveViewDown{
    if(!_viewUP) return;
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = CGRectMake(self.view.frame.origin.x,
                                     self.view.frame.origin.y + 220,
                                     self.view.frame.size.width,
                                     self.view.frame.size.height);
    } completion:^(BOOL finished){
        _viewUP = NO;
    }];
}
@end
