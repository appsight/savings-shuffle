//
//  DealTableViewCell.h
//  SavingSuffle
//
//  Created by ajay desai on 07/04/15.
//  Copyright (c) 2015 Umer Hassam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DealTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *dealImage;
@property (weak, nonatomic) IBOutlet UILabel *dealTitle;
@property (weak, nonatomic) IBOutlet UILabel *dealQty;
@property (weak, nonatomic) IBOutlet UILabel *Date_lbl;
@property (weak, nonatomic) IBOutlet UILabel *Time_lbl;

@end
